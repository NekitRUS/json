<?php
function newJSON($oldObject){
    $newFormat = array();
    $newFormat['first_name'] = $oldObject['first_name'];
    $newFormat['last_name'] = $oldObject['last_name'];
    $newFormat['second_name'] = $oldObject['second_name'];
    $newFormat['name']= ($oldObject['name']);
    $newFormat['phone'] = $oldObject['phone'];
    $newFormat['password'] = $oldObject['password'];
    $newFormat['email'] = $oldObject['email'];
    $newFormat['registration_date'] = $oldObject['registration_date'];
    foreach ($newFormat as $key => $value){
        $newFormat[$key] =  replace_I($value);
    }
      
    $school = array();
    $school['grade'] = $oldObject['school_grade'];
    $school['region'] = $oldObject['region']; 
    $school['town'] = $oldObject['town'];
    $school['address'] = RemoveTown(trim($oldObject['school_address']), $oldObject['town']);
    $school['number'] = $oldObject['school_number'];
    $school['zipcode'] = $oldObject['school_zipcode'];
    $school['site'] = RightAddress($oldObject['school_site']);
    foreach ($school as $key => $value){
        $school[$key] = replace_I($value);
    }
    $school['teachers'] = Teachers($oldObject['school_teacher1'], $oldObject['school_teacher2']);
    $newFormat['school_info'] = $school;
    return $newFormat;
}
    
function replace_I($obj){
    return mb_ereg_replace(strval(chr(239).chr(191).chr(189).chr(63)), iconv("cp1251", "UTF-8", '�'), $obj);
}
    
function RightAddress($str){
    if(substr($str, 0, 7) == 'http://'){
        $str = str_replace("http://", "", $str);
    }
    if(substr($str, 0, 4) == 'www.'){
        $str = str_replace("www.", "", $str);
    }
    $str = 'http://www.' . $str;
        
    return CheckSite($str) ? $str : iconv("cp1251", "UTF-8", '�� ������');           
}
    
function CheckSite ($url) {
    if(filter_var($url, FILTER_VALIDATE_URL) == FALSE){
        return false;
    }
    $check =  get_headers($url);
    if(!$check){
        return false;
    }
    return (explode(" ", $check[0])[1] == "200");
}
    
function RemoveTown($str, $townName) {
    $str = str_replace($townName, "", $str);
    $index = strpos($str, ', ');
    if($index === 0){
        $str = substr($str, 2);
    }
    return $str;
}
    
function Teachers($field1, $field2){
    $teachers1 = explode(', ', $field1);
    foreach ($teachers1 as $key => $value){
        $teachers1[$key] = replace_I($value);
    }
    $teachers2 = explode(', ', $field2);
    foreach ($teachers2 as $key => $value){
        $teachers2[$key] = replace_I($value);
    }
    if($teachers1[0] == $teachers2[0]){
        $keys = array();
        for($i=1; $i <= count($teachers2); $i++){
            $keys[] = $i;
        }
        return array_combine($keys, $teachers2);
    }
    else{
        $keys = array();
        for($i=1; $i<=count($teachers1) + count($teachers2); $i++){
            $keys[] = $i;
        }
        return array_combine($keys, array_merge($teachers1, $teachers2));
    }
}